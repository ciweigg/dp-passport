package net.chenlin.dp.ids.server.manager;

import net.chenlin.dp.ids.common.entity.SessionData;

/**
 * session管理器
 * @author zcl<yczclcn@163.com>
 */
public interface SessionManager {

    /**
     * 更新session过期时间
     * @param sessionId
     * @param loginType
     */
    void update(String sessionId, Integer loginType);

    /**
     * 获取sessionData
     * @param sessionId
     * @param loginType
     * @return
     */
    SessionData get(String sessionId, Integer loginType);

    /**
     * 保存session
     * @param sessionId
     * @param sessionData
     */
    void save(String sessionId, SessionData sessionData);

    /**
     * 删除session
     * @param sessionId
     * @param loginType
     */
    void remove(String sessionId, Integer loginType);

}
